<?php

namespace Drupal\db_custom_tools;

use Drupal\Core\Render\Markup;

class DbCustomToolsTwigExtension extends \Twig_Extension {

  /**
   * This is the same name we used on the services.yml file
   * @return string
   */
  public function getName() {
    return "db_custom_tools.twig_extension";
  }

  /**
   * Here is where we declare our new filter.
   * @return array
   */
  public function getFilters() {
    return array(
      'basedomain' => new \Twig_Filter_Function(
        array('Drupal\db_custom_tools\DbCustomToolsTwigExtension', 'baseDomainFilter') // Here we are self referencing the function we use to filter the string value.
      ),
      'dashify' => new \Twig_Filter_Function(
        array('Drupal\db_custom_tools\DbCustomToolsTwigExtension', 'dashifyFilter') // Here we are self referencing the function we use to filter the string value.
      )
    );
  }

  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('basedomain', [$this, 'baseDomainFilter']),
      new \Twig_SimpleFunction('dashify', [$this, 'dashifyFilter']),
    ];
  }

  /**
   * @param $string
   * @return float
   */
  public static function baseDomainFilter($string) {
    if($string) {
      $urlParse = parse_url($string);
      $domainValue = $urlParse['host'];
      return $domainValue;
    } else {
      return $string;
    }
  }
  public static function dashifyFilter($string) {
    if($string) {
      $lower = strtolower($string);
      $spaced = str_replace(' ', '-', $lower);
      $dashed = str_replace('_', '-', $spaced);
      $cleaned = preg_replace('/[^A-Za-z0-9-]/', '', $dashed);
      return $cleaned;
    } else {
      return $string;
    }
  }
}