Hi. I'm hoping that I will keep this up to date with the features and customizations within this module. Let's get going.

October 29, 2020
--Added automatic filter to search API page results to remove view embed shortcodes
--Search API Pages now display search terms in the page title

October 8, 2020

--Alters media in the search reults to link to the file instead of the media item

--Adds SR only text to the search form

--Fixes jQuery UI accordions on the admin side where the stupid caret is on its own line

--Adds custom Twig filters
----basedomainfilter
------grabs and displays the base domain of a URL
------Use in a view rewrite like so: {{ basedomainfilter(field_name) }}
------https://blah.wa.gov/pagename/pagename2 becomes https://blah.wa.gov
----dashify
------Simply rewrites a term machine name to a dashed version
------Use in a view rewrite like so: {{ dashify(field_name) }}
------term_machine_name becomes term-machine-name

--Attaches the theme's global CSS to all Drupal forms

--Attaches the theme's global CSS to the link edit dialog
----Hides the default Add Media link
